import pandas as pd
import toolforge as forge
import duckdb
from datetime import datetime, date

user_agent = forge.set_user_agent(
    tool="Automoderator Baseline Metrics Pipeline",
    url="https://gitlab.wikimedia.org/kcvelaga/automoderator-baseline-pipelines-mariadb",
    email="kcvelaga@wikimedia.org",
)

duck_conn = duckdb.connect('fr_pending_stats.db')

duck_conn.execute("""
CREATE TABLE IF NOT EXISTS fr_pending_hourly (
    wiki_db VARCHAR NOT NULL,
    date DATE NOT NULL,
    hour INTEGER NOT NULL,
    pending_frevs INTEGER,
    avg_elapsed_secs INTEGER
)
""")

# selected wikis from https://noc.wikimedia.org/conf/highlight.php?file=dblists/flaggedrevs.dblist
wikis=[
    "alswiki",
    "arwiki",
    "bewiki",
    "bnwiki",
    "bswiki",
    "cewiki",
    "ckbwiki",
    "dewiki",
    "enwiki",
    "eowiki",
    "fawiki",
    "fiwiki",
    "hiwiki",
    "huwiki",
    "iawiki",
    "idwiki",
    "kawiki",
    "mkwiki",
    "plwiki",
    "ruwiki",
    "sqwiki",
    "trwiki",
    "ukwiki",
    "vecwiki",
    "zh_classicalwiki",
]

with open('calculate_fr_pending_hourly.sql', 'r') as query_file:
    query = query_file.read()
    
fr_pending = []
cols = ['wiki_db', 'date', 'hour', 'pending_frevs', 'avg_elapsed_secs']

curr_dt = datetime.now().date()
curr_hour = datetime.now().hour

logs_df = pd.read_csv('logs.tsv', sep='\t')
curr_logs = {}

for wiki in wikis:
    try:
        curr_logs = {
            'wiki_db': wiki,
            'date': curr_dt,
            'hour': curr_hour,
            'is_fetch_successful': False,
            'fetch_error': None,
            'is_processing_successful': False,
            'process_error': None,
            'is_db_write_successful': False,
            'db_write_error': None
        }

        conn = forge.connect(wiki)
        with conn.cursor() as cur:
            cur.execute(query.format(wiki_db=wiki))
            result = cur.fetchall()
            cur.close()
        conn.close()

        curr_logs['is_fetch_successful'] = True

        fr_pending_wiki = pd.DataFrame(result, columns=cols)
        fr_pending_wiki['wiki_db'] = wiki

        curr_logs['is_processing_successful'] = True

        for index, row in fr_pending_wiki.iterrows():
            duck_conn.execute("""
            DELETE FROM fr_pending_hourly
            WHERE wiki_db = ? AND date = ? AND hour = ?
            """, (row['wiki_db'], row['date'], row['hour']))

            duck_conn.execute("""
            INSERT INTO fr_pending_hourly (wiki_db, date, hour, pending_frevs, avg_elapsed_secs)
            VALUES (?, ?, ?, ?, ?)
            """, (row['wiki_db'], row['date'], row['hour'], row['pending_frevs'], row['avg_elapsed_secs']))

        curr_logs['is_db_write_successful'] = True

    except Exception as e:
        error_text = str(e)
        if not curr_logs['is_fetch_successful']:
            curr_logs['fetch_error'] = error_text
        elif not curr_logs['is_processing_successful']:
            curr_logs['process_error'] = error_text
        else:
            curr_logs['db_write_error'] = error_text

    logs_df = logs_df._append(curr_logs, ignore_index=True)

duck_conn.close()

logs_df.to_csv('logs.tsv', sep='\t', index=False)
