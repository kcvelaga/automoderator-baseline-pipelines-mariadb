import duckdb
import pandas as pd

duck_conn = duckdb.connect('fr_pending_stats.db')

query = """
SELECT 
    * 
FROM 
    fr_pending_hourly
ORDER BY
    date DESC, hour DESC
LIMIT 25
"""

print(duck_conn.sql(query).df())
duck_conn.close()
